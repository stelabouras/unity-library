;(function() {

  this.unveil = function(threshold, callback){
    var w = window,
        th = threshold || 0,
        images = [].slice.call(document.querySelectorAll("img.unveil"));

    if ("IntersectionObserver" in window 
      && "IntersectionObserverEntry" in window 
      && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {

        let options = {
          rootMargin: `${threshold}px 0px`
        }
        let lazyImageObserver = new IntersectionObserver(unveil, options);

        images.forEach(function(img) {
          lazyImageObserver.observe(img);
        });
    
      function unveil(changes, observer) {
        changes.forEach(function(change) {
          if (change.isIntersecting) {
            let img = change.target;
            let imgSrc = new Image();
            imgSrc.onload = function() {
              change.target.src = this.src;
            };
            imgSrc.src = img.dataset.src;

            //img.src = img.dataset.src;
            //img.srcset = img.dataset.srcset;
            img.classList.remove("unveil");
            observer.unobserve(img);
            if (typeof callback === "function") callback.call(img);
          }
        });
      }
      
    } else {

      console.log('Intersection API not supported!');
      // Fallback to vanilla-js rewrite of 'unveil.js' http://luis-almeida.github.io/unveil/

      // test for browser support of `once` option for events
      // https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#Safely_detecting_option_support
      var onceSupported = false;
      try {
        var options = {
          get once() {
            onceSupported = true;
          }
        };
        w.addEventListener("test", options, options);
        w.removeEventListener("test", options, options);
      } catch(err) {
        onceSupported = false;
      }

      images.forEach(function(image){
        image.addEventListener('unveil', function(){ 
          var img = this;
          var source = img.getAttribute("data-src");
          var sourceset = img.getAttribute("data-srcset")
          img.setAttribute("src", source);
          if (sourceset) {
            img.setAttribute("srcset", sourceset);
          };
          img.classList.remove("unveil");
          if (typeof callback === "function") callback.call(img);
        }, onceSupported ? { once: true } : false);
      });

      var debouncedUnveil = debounce(unveil, 100);

      function unveil() {
        // create an array of images that are in view
        // by filtering the intial array
        var inview = images.filter(function(img) {
          // if the image is set to display: none
          if (img.style.display === "none") return;

          var rect = img.getBoundingClientRect(), 
              wt = window.scrollY, // window vertical scroll distance
              wb = wt + w.innerHeight, // last point of document visible in browser window
              et = wt + rect.top, // distance from document top to top of element
              eb = wt + rect.bottom; // distance from top of document to bottom of element

          // the bottom of the element is below the top of the browser (- threshold)
          // && the top of the element is above the bottom of the browser (+ threshold)
          return eb >= wt - th && et <= wb + th;
        });

        if (w.CustomEvent) {
          var unveilEvent = new CustomEvent('unveil');
        } else {
          var unveilEvent = document.createEvent('CustomEvent');
          unveilEvent.initCustomEvent('unveil', true, true);
        }

        inview.forEach(function(inviewImage){
          inviewImage.dispatchEvent(unveilEvent);
        });
        // alternative -- two arrays: https://stackoverflow.com/questions/11731072/dividing-an-array-by-filter-function
        // another possibility -- use getElementsByClassName which returns _live_ HTML Collection
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/getElementsByClassName
        images = [].slice.call(document.querySelectorAll("img.unveil"));
      }

      // https://davidwalsh.name/javascript-debounce-function
      // from underscore.js
      function debounce(func, wait, immediate) {
        var timeout;
        return function() {
          var context = this, 
              args = arguments;
          var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      };

      w.addEventListener('scroll', debouncedUnveil);
      w.addEventListener('resize', debouncedUnveil);

      unveil();
    }

  }
}());

let LibrarySearcher = (function() {

  let searchAndRender = function(token) {

    this.close(false);

    let results = search(token);
    let renderedHTML = render(results);

    let existingArticleNode = document.querySelector('article');

    let searchHeader = document.createElement('h2');

    var html = '<a class="previous" href="javascript:void(0);" onclick="LibrarySearcher.close(true);">'
    html += '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.25" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"">'
    html += '<circle cx="12" cy="12" r="9"></circle>'
    html += '<line x1="8" y1="12" x2="16" y2="12"></line>'
    html += '<line x1="8" y1="12" x2="12" y2="16"></line>'
    html += '<line x1="8" y1="12" x2="12" y2="8"></line>'
    html += '</svg>'
    html += '</a>'
    html += 'Search for \'' + token + '\' (' + results.total + ')'

    searchHeader.innerHTML = html

    searchHeader.className = 'search-header'

    document.body.querySelector('header').appendChild(searchHeader)

    let searchContainer = document.createElement('article');
    searchContainer.className = 'search-results';
    searchContainer.innerHTML =  renderedHTML;

    document.body.insertBefore(searchContainer, existingArticleNode.nextSibling);

    document.body.className = 'search-enabled';

    unveil(200, function() { this.style.opacity = 1.0; });    
  }

  let close = function(shouldClearInput) {

    if(document.querySelector('.search-results') == undefined)
      return;

    document.body.className = '';
    document.querySelector('.search-results').parentNode.removeChild(document.querySelector('.search-results'));
    document.querySelector('.search-header').parentNode.removeChild(document.querySelector('.search-header'));

    if(shouldClearInput)
      document.querySelector('.search-field input').value = '';
  }

  let search = function(token) {

    if(library === undefined)
      return;

    let outputFolder = 'output';
    var results = {
      'folders': [],
      'links': [],
      'total': 0
    };

    var total = 0;

    let tokenNormalized = token.toLowerCase();

    library.forEach(section => {

      if(section.title != undefined
        && section.title.toLowerCase().indexOf(tokenNormalized) > -1) {

        var sectionLink = section.file;

        if(section.file.startsWith(outputFolder + '/'))
          sectionLink = sectionLink.substring(outputFolder.length);

        var folderResult = {
          'title': section.title,
          'link': sectionLink
        };

        if(section['slugname'] != undefined)
          folderResult['slugname'] = section['slugname'];

        results['folders'].push(folderResult);

        total += 1;
      }

      if(section.links != undefined) {

        section.links.forEach(link => {

          if(link['type'] != 'link')
            return;

          let linkName = link.name.toLowerCase();
          let domain = getDomainName(link['link']);

          if(linkName.indexOf(tokenNormalized) > -1 ||
            domain.indexOf(tokenNormalized) > -1) {

            var linkResult = {
              'title': link.name,
              'link': link.link
            };

            if(link.image != undefined)
              linkResult['image'] = link.image;

            if(link.wayback != undefined)
              linkResult['wayback'] = link.wayback;

            results['links'].push(linkResult);

            total += 1;
          }
        });
      }
    });

    results['total'] = total;

    return results;
  }

  let getDomainName = function(hostName) {
    var    a      = document.createElement('a');
         a.href = hostName;
    return a.hostname;
  }

  let render = function(results) {

    var html = '';

    let folders = results['folders'];

    if(folders != undefined 
      && folders.length > 0) {

      html += '<section class="folders">'

      folders.forEach(folder => {
        html += '<a href="' + folder['link'] + '"'

        if(folder['slugname'] != undefined)
          html += ' class="' + folder['slugname'] + '"'

        html += '>' + folder['title'] + '</a>'
      });

      html += '</section>'
    }

    let links = results['links'];

    if(links != undefined 
      && links.length > 0) {

      html += '<section class="links">'

      links.forEach(link => {

        if(link.wayback != undefined)
          html += '<a href="' + link['link'] + '" data-orighref="' + link['link'] + '" data-wbhref="' + link['wayback'] + '">'
        else
          html += '<a href="' + link['link'] + '">'

        html += '<div class="img-container">'
        html += '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"'

        if(link.image != undefined){
          html += ' class="unveil" data-src="' + link['image'] + '"'
        }

        html += '/>'
        html += '</div>'
        html += '<div class="info">'
        html += '<span>'

        html += link['title']

        html += '</span>'
        html += '<em>' + getDomainName(link['link']) + '</em>'

        html += '</div>'

        if(link.wayback != undefined)
          html += '<i title="Wayback Machine archived link" onmouseenter="let el=this.parentNode;el.href=el.dataset.wbhref;" onmouseleave="let el=this.parentNode;el.href=el.dataset.orighref;">WM</i>' 

        html += '</a>'
      });

      html += '</section>'
    }

    return html
  }

  return {
    'searchAndRender': searchAndRender,
    'close': close
  }
})();

window.addEventListener('DOMContentLoaded', () => {
  window.unveil(200, function() { this.style.opacity = 1.0; });
});

document.addEventListener('keyup', (e) => {
  if(e.keyCode == 27) {
    LibrarySearcher.close(true);
  }
  else if(e.keyCode == 13) {

    var searchField = undefined

    if(document.querySelector('.search-results .search-field input') != undefined)
      searchField = document.querySelector('.search-results .search-field input')      
    else if(document.querySelector('.search-field input') != undefined)
      searchField = document.querySelector('.search-field input')

    if(searchField == undefined)
        return;

    let searchText = searchField.value;

    if(searchText.length <= 1)
        return;

    LibrarySearcher.searchAndRender(searchText);
  }
});
