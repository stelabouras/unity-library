# Unity Library

A curated library of Unity related articles, fully searchable, with support for Wayback Machine.

The library is currently hosted on the static website service Netlify.

You can access the live version here: [https://unity.stelabouras.com/](https://unity.stelabouras.com/).

## Add links to the library

In order to add a link to the library, locate the `input/` folder and the associated directory/category (e.g. `Fire/`, `Liquid/` etc) that your link can be a part of, and then drag-n-drop the link from your browser to that folder so that a new `.webloc` or `.url` file is generated.

After you are done adding links, you only have to run the `librarian.py` script (as shown below) so that the metadata can be extracted and you can `git add` all those changes to the commit.

## Generate the library locally

Install `virtualenv` and then the required python libraries (`requirements.txt`) and on the root folder run:

```
. env/bin/activate.fish
python librarian.py -i input/ -o output/
python generator.py -i library.json -o output/ -s static/
```

The `librarian.py` script extracts the metadata and adds the url to the Wayback Machine and generates the `library.json` file which then is fed to the `generator.py` script that generates the website.

Netlify only runs the `generator.py` script as running the `librarian.py` on the server for the whole library each time can take quite a while to finish.

### License

Shader Graph Viewer is licensed under the [MIT License](https://gitlab.com/stelabouras/unity-library/-/blob/master/LICENSE).
