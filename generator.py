from os.path import join
from os import listdir, walk, path
import os
import sys, getopt
import json
from urllib.parse import urlparse
import shutil
from shutil import copyfile
from datetime import datetime
import collections

def get_domain(link):
    """Parse the domain from the URL."""
    parsed_url = urlparse(link)

    if parsed_url is None:
        return None

    if parsed_url.hostname is None:
        return None

    return parsed_url.hostname.replace("www.", "")

def generateChangelong(inputFile, outputFolder):

	library = None

	try:
		with open(inputFile) as json_file:
			library = json.load(json_file)
	except:
		pass

	if library == None or 'entries' not in library:
		print('File not found, exiting...')
		return

	flatMap = {}

	for section in library['entries']:

		links = section.get('links') or None

		if links != None and len(links) > 0:
			for link in links:

				link['sectiontitle'] = section['title']

				created = link['created']

				if created not in flatMap:
					flatMap[created] = []

				flatMap[created].append(link)

	flatMap = collections.OrderedDict(sorted(flatMap.items(), reverse=True))

	changelog = ""

	for day in flatMap:

		date = datetime.strptime(day, '%Y%m%d')

		changelog += date.strftime("%Y/%m/%d") + '\n'
		changelog += "---\n"

		for entry in flatMap[day]:

			changelog += '* "'

			if entry['type'] == 'image':
				changelog += entry['title']
			elif 'name' in entry:
				changelog += entry['name']

			changelog += '" in "' +  entry['sectiontitle'] + '"\n'

		changelog += '\n'

	newFile = path.join(outputFolder, 'changelog.txt')

	f = open(newFile, 'w+')
	f.write(changelog)
	f.close()

def generateWebsite(inputFile, outputFolder, staticFolder):

	library = None

	try:
		with open(inputFile) as json_file:
			library = json.load(json_file)
	except:
		pass

	if library == None or 'entries' not in library:
		print('File not found, exiting...')
		return

	generatedDate = 'Unknown'

	if 'generated' in library:
		generatedDate = datetime.utcfromtimestamp(float(library['generated'])).strftime('%Y-%m-%d %H:%M:%S')

	if path.exists(outputFolder):
		shutil.rmtree(outputFolder)

	staticFolderOutput = path.join(outputFolder, staticFolder)

	shutil.copytree(staticFolder, staticFolderOutput)

	projectTitle = 'The Unity Library'

	for section in library['entries']:

		newPath = section['path']
		newFile = section['file']
		pageTitle = section.get('title') or None
		parentFolder = section.get('parent') or None
		folders = section.get('folders') or None 
		links = section.get('links') or None

		if not path.exists(newPath):
		    os.makedirs(newPath)

		f = open(newFile, 'w+')

		if pageTitle == None:
			print('Generating homepage...' + newFile)
		else:
			print('Generating ' + pageTitle + '...' + newFile)

		html = ''

		html += '<!doctype html>'
		html += '<html>'
		html += '<head>'
		html += '<meta charset="utf-8"/>'
		html += '<meta http-equiv="cleartype" content="on"/>'
		html += '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>'
		html += '<meta name="HandheldFriendly" content="True"/>'
		html += '<meta name="MobileOptimized" content="320"/>'

		if pageTitle != None:
			html += '<title>' + projectTitle + ' : ' + pageTitle + '</title>'
		else:
			html += '<title>' + projectTitle + '</title>'

		html += '<link href="/static/styles/main.css" rel="stylesheet"/>'
		html += '<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:100,200,300,400,700,900&subset=latin" rel="stylesheet" type="text/css"/>'
		html += '<link rel="apple-touch-icon" sizes="180x180" href="/static/favicon/apple-touch-icon.png">'
		html += '<link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png">'
		html += '<link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png">'
		html += '<link rel="manifest" href="/static/favicon/site.webmanifest">'
		html += '<meta name="msapplication-TileColor" content="#FFF6DD">'
		html += '<meta name="theme-color" content="#FFF6DD">'
		html += '</head>'
		html += '<body>'

		html += '<header>'

		html += '<h1>'

		html += '<svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="50px" height="50px">'
		html += '<path d="M 44.082031 25.207031 L 46.902344 20.429688 C 47.015625 20.191406 47.03125 19.917969 46.945313 19.671875 L 41.359375 3.671875 C 41.1875 3.183594 40.675781 2.90625 40.179688 3.027344 L 23.761719 7.027344 C 23.449219 7.105469 23.191406 7.328125 23.070313 7.628906 L 21.324219 12 L 16 12 C 15.722656 12 15.453125 12.117188 15.265625 12.320313 L 3.265625 25.320313 C 2.894531 25.722656 2.914063 26.347656 3.308594 26.722656 L 15.894531 38.722656 C 16.078125 38.902344 16.328125 39 16.582031 39 L 22.320313 39 L 24.070313 43.371094 C 24.199219 43.695313 24.488281 43.925781 24.828125 43.984375 L 42.289063 46.984375 C 42.34375 46.996094 42.402344 47 42.457031 47 C 42.921875 47 43.332031 46.679688 43.433594 46.214844 L 46.976563 29.984375 C 47.03125 29.734375 46.988281 29.472656 46.851563 29.25 Z M 21.324219 12 L 35.417969 9.417969 L 28 24 L 11.167969 24 Z M 22.324219 39 L 11.167969 28 L 28 28 L 36.75 41.167969 Z M 40.386719 38.898438 L 32 26 L 39.332031 11.417969 L 44.082031 25.210938 Z"/>'
		html += '</svg>'

		if pageTitle != None:
			html += '<a href="/">' + projectTitle + '</a>'
		else:
			html += '<span>' + projectTitle + '</span>'

		html += '</h1>'

		html += '<section class="search-field"><input type="search" autocomplete="off" value="" placeholder="Search..."></section>'

		if pageTitle != None:

			html += '<h2>'

			if parentFolder != None and parentFolder + '/' != outputFolder:
				html += '<a class="previous" href="/' + path.join(parentFolder, 'index.html') + '">'
			else:
				html += '<a class="previous" href="/">'

			html += '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.25" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"">'
			html += '<circle cx="12" cy="12" r="9"></circle>'
			html += '<line x1="8" y1="12" x2="16" y2="12"></line>'
			html += '<line x1="8" y1="12" x2="12" y2="16"></line>'
			html += '<line x1="8" y1="12" x2="12" y2="8"></line>'
			html += '</svg>'
			html += '</a>'

			html += pageTitle
			html += '</h2>'

		html += '</header>'

		html += '<article>'

		sectionLetter = None

		if folders != None and len(folders) > 0:

			if pageTitle == None:
				html += '<section class="folders homepage">'
			else:
				html += '<section class="folders">'

			for folder in folders:

				if pageTitle == None and (sectionLetter is None or folder['name'][0] != sectionLetter):
					sectionLetter = folder['name'][0]
					html += '<h4>' + sectionLetter + '</h4>'

				html += '<a href="/' + folder['link'] + '"'

				if pageTitle == None:
					html += ' class="' + folder['slugname'] + '"'

				html += '>'
				html += folder['name']
				html += '</a>'

			html += '</section>'

		if links != None and len(links) > 0:

			html += '<section class="links">'

			links = sorted(links, key=lambda k: k['created'], reverse=True) 

			if links != None and len(links) > 0:
				for link in links:

					if link['type'] == 'link':

						if 'wayback' in link:
							html += '<a href="' + link['link'] + '" data-orighref="' + link['link'] + '" data-wbhref="' + link['wayback'] + '">'
						else:
							html += '<a href="' + link['link'] + '">'

						html += '<div class="img-container">'
						html += '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"'

						if 'image' in link:
							html += ' class="unveil" data-src="' + link['image'] + '"'

						html += '/>'
						html +=	'</div>'
						html += '<div class="info">'
						html += '<span>'

						html += link['name']

						html += '</span>'
						html += '<em>' + get_domain(link['link']) + '</em>'

						html += '</div>'

						if 'wayback' in link:
							html += '<i title="Wayback Machine archived link" onmouseenter="let el=this.parentNode;el.href=el.dataset.wbhref;" onmouseleave="let el=this.parentNode;el.href=el.dataset.orighref;">WM</i>' 

						html += '</a>'

					else:

						srcPath = link['input'];				
						newImage = path.join(newPath, link['src'])

						copyfile(srcPath, newImage)

						html += '<a href="' + link['src'] + '">'
						html += '<div class="img-container">'
						html += '<img class="unveil" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"'
						html += ' data-src="' + link['src'] + '"'
						html += '/>'
						html +=	'</div>'
						html += '<div class="info">'
						html += '<span>'

						html += link['title']

						html += '</span>'
						html += '</div>'
						html += '</a>'

			html += '</section>'

		html += '</article>'
		html += '<footer>Copyright 2020 | <a href="https://twitter.com/stelabouras">@stelabouras</a> | <a href="mailto:hello@stelabouras.com?subject=Unity%20Library%20Suggestion">Suggest a link</a> | <a href="/changelog.txt">Changelog</a> | Generated at ' + generatedDate + ' UTC</footer>'
		html += '<script>library = ' + json.dumps(library['entries']) + ';</script>'
		html += '<script src="/static/scripts/script.js"></script>'
		html += '</body>'
		html += '</html>'

		f.write(html)
		f.close()

def main(argv):
	inputfile = ''
	outputfolder = ''
	staticfolder = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:s:",["ifile=","ofile=","sfile="])
	except getopt.GetoptError as e:
		print('runner.py -i <inputfile> -o <outputfolder> -s <staticfolder>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('runner.py -i <inputfile> -o <outputfolder> -s <staticfolder>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfolder = arg
		elif opt in ("-s", "--sfile"):
			staticfolder = arg

	generateWebsite(inputfile, outputfolder, staticfolder)
	generateChangelong(inputfile, outputfolder)

if __name__ == "__main__":
   main(sys.argv[1:])
