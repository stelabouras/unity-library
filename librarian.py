from os import listdir, walk, path
from os.path import isfile, join
import os
import sys, getopt
from slugify import slugify
import re
import webloc
import json
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup
import shutil
from shutil import copyfile
import hashlib
import time
import datetime as dt

def get_domain(link):
    """Parse the domain from the URL."""
    parsed_url = urlparse(link)

    if parsed_url is None:
        return None

    if parsed_url.hostname is None:
        return None

    return parsed_url.hostname.replace("www.", "")

def parse_link_from_file(contents):

    url = None
    weblocRE = "URL\<\/key\>.*?\<string\>(?P<url>.*?)\<\/string\>"
    urlRE = "URL\=(?P<url>.*?)[\r\n]*$"

    if re.search(urlRE, contents, re.MULTILINE):
        url = re.search(urlRE, contents, re.MULTILINE).groupdict()["url"]
    elif re.search(weblocRE, contents, re.DOTALL):
        url = re.search(weblocRE, contents, re.DOTALL).groupdict()["url"]

    return url

def linkhash(link):
	return hashlib.md5(link.encode('utf-8')).hexdigest()

def is_valid_extension(file_extension):
    """Ignore files without a .webloc / .url / .website extension."""
    valid_extensions = [".webloc", ".url", ".website"]
    return (file_extension.lower() in valid_extensions)

def timestamptoyyymmdd(timestamp):
	return dt.datetime.utcfromtimestamp(timestamp).strftime("%Y%m%d")

def parseFolder(inputFolder, outputFolder):

	loadedCache = None

	try:
		with open('cache.json') as json_file:
			loadedCache = json.load(json_file)
	except:
		pass

	cache = {}
	library = {}
	entries = []

	for (dirpath, dirnames, filenames) in walk(inputFolder):

		relPath = path.relpath(dirpath, inputFolder)	

		if relPath == '.':
			relPath = ''

		slugifiedPathComps = []

		for pathComp in path.split(relPath):

			if len(pathComp) > 0:
				slugifiedPathComps.append(slugify(pathComp))

		if len(slugifiedPathComps) > 0:
			relPath = path.join(*slugifiedPathComps)

		newPath = path.join(outputFolder, relPath)

		if not path.exists(newPath):
		    os.makedirs(newPath)

		newFile = path.join(newPath, 'index.html')

		pageTitle = None

		if dirpath != inputFolder:
			pageTitle = path.basename(dirpath)

		parentFolder = path.split(path.split(newPath)[-2])[-1]

		librarySection = {
			'path' : newPath,
			'file' : newFile,
		}

		if newPath != outputFolder:
			librarySection['parent'] = parentFolder

		if pageTitle != None:
			librarySection['title'] = pageTitle
			librarySection['slugname'] = slugify(pageTitle)
			
		if len(dirnames) > 0:

			librarySectionFolders = []

			for dirname in sorted(dirnames):

				slugName = slugify(dirname)
				folderLink = path.relpath(path.join(newPath, slugName,'index.html'), outputFolder)

				librarySectionFolders.append({
					'name' : dirname,
					'link' : folderLink,
					'slugname': slugName
				})

			librarySection['folders'] = librarySectionFolders

		if len(filenames) > 0:

			librarySectionLinks = []

			for filename in sorted(filenames):

				fn, file_extension = path.splitext(filename)

				fullFilename = path.join(dirpath, filename)

				if not is_valid_extension(file_extension):

					if file_extension == '.png':

						# Copy file to file system
						sectionImage = {
							'type': 'image',
							'input': fullFilename,
							'src': filename,
							'title': fn
						}

						birthDate = None

						stat = os.stat(fullFilename)

						try:
							birthDate = stat.st_birthtime
						except:
							pass

						if birthDate is not None:
							sectionImage['created'] = timestamptoyyymmdd(birthDate)

						librarySectionLinks.append(sectionImage)

					continue

				link = None

				if file_extension == '.webloc':
					link = webloc.read(fullFilename)
				else:
					file = open(fullFilename, 'r', encoding="utf-8")
					contents = file.read()
					link = parse_link_from_file(contents)

				if link == None:
					continue

				cacheKey = linkhash(link)

				graph = None

				librarySectionLink = {}

				print('Fetching ' + link + ' (' + fn + ')...')

				linkTitle = None

				if cacheKey in loadedCache:

					print('Cache hit for ' + link)
					
					loadedCacheElement = loadedCache[cacheKey]

					linkTitle = loadedCacheElement['title']

					if 'wayback' in loadedCacheElement:
						librarySectionLink['wayback'] = loadedCacheElement['wayback']

					if 'image' in loadedCacheElement:
						librarySectionLink['image'] = loadedCacheElement['image']

				else:

					# Fetch OpenGraph tags if available
					response = None

					try:
						response = requests.get(link)
					except KeyboardInterrupt:
						print('Interrupted')
						try:
							sys.exit(0)
						except SystemExit:
							os._exit(0)           
					except:
						pass

					doc = None
					params = {}

					if response != None and len(response.text) > 0:
						try:
							doc = BeautifulSoup(response.text, features="html.parser")
						except KeyboardInterrupt:
							print('Interrupted')
							try:
								sys.exit(0)
							except SystemExit:
								os._exit(0)           
						except:
							pass

					if doc != None and doc.head != None:
						ogs = doc.head.findAll(property=re.compile(r'^og'))
						for og in ogs:
							if og.has_attr(u'content'):
								params[og[u'property'][3:]]=og[u'content']

					linkTitle = fn

					if 'title' in params and len(params['title']) > 0:
						linkTitle = params['title']

					if 'image' in params and len(params['image']) > 0:
						librarySectionLink['image'] = params['image']

					# Try to cache the page in the Wayback Machine
					waybackResponse = None

					try:
						waybackResponse = requests.get("https://web.archive.org/save/" + link)
					except KeyboardInterrupt:
						print('Interrupted')
						try:
							sys.exit(0)
						except SystemExit:
							os._exit(0)           
					except:
						pass

					if waybackResponse != None and waybackResponse.status_code == 200 and "content-location" in waybackResponse.headers and not waybackResponse.headers["content-location"].startswith('/save'):
						librarySectionLink['wayback'] = "https://web.archive.org" + waybackResponse.headers["content-location"]
					elif waybackResponse != None:
						print('Wayback error! ' + str(waybackResponse.status_code))
					else:
						print('Wayback generic error!')

				print('FETCHED! -> ' + linkTitle)

				librarySectionLink['name'] = linkTitle
				librarySectionLink['link'] = link
				librarySectionLink['type'] = 'link'

				birthDate = None

				stat = os.stat(fullFilename)

				try:
					birthDate = stat.st_birthtime
				except:
					pass

				if birthDate is not None:
					librarySectionLink['created'] = timestamptoyyymmdd(birthDate)

				librarySectionLinks.append(librarySectionLink)

				cacheValue = {
					'title' : linkTitle
				}

				if 'image' in librarySectionLink:
					cacheValue['image'] = librarySectionLink['image']

				if 'wayback' in librarySectionLink:
					cacheValue['wayback'] = librarySectionLink['wayback']

				cache[cacheKey] = cacheValue

			if len(librarySectionLinks) > 0:
				librarySection['links'] = librarySectionLinks

		entries.append(librarySection)

		library['entries'] = entries
		library['generated'] = str(time.time())

	with open('library.json', 'w') as f:
	    json.dump(library, f)

	with open('cache.json', 'w') as f:
	    json.dump(cache, f)

	print(library)

	print('---')

	print(cache)

def main(argv):
	inputfolder = ''
	outputfolder = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
	except getopt.GetoptError as e:
		print('librarian.py -i <inputfolder> -o <outputfolder>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('librarian.py -i <inputfolder> -o <outputfolder>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfolder = arg
		elif opt in ("-o", "--ofile"):
			outputfolder = arg

	parseFolder(inputfolder, outputfolder)

if __name__ == "__main__":
   main(sys.argv[1:])
